--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.13
-- Dumped by pg_dump version 10.4

-- Started on 2018-06-22 00:58:24 EEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12361)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2137 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 183 (class 1259 OID 16438)
-- Name: artists; Type: TABLE; Schema: public; Owner: service_role
--

CREATE TABLE public.artists (
    artist_id uuid NOT NULL,
    first_name character varying,
    last_name character varying,
    stage_name character varying
);


ALTER TABLE public.artists OWNER TO service_role;

--
-- TOC entry 185 (class 1259 OID 16461)
-- Name: merchendise; Type: TABLE; Schema: public; Owner: service_role
--

CREATE TABLE public.merchendise (
    product_id uuid NOT NULL,
    artist_id uuid NOT NULL,
    name character varying
);


ALTER TABLE public.merchendise OWNER TO service_role;

--
-- TOC entry 182 (class 1259 OID 16429)
-- Name: product_types; Type: TABLE; Schema: public; Owner: service_role
--

CREATE TABLE public.product_types (
    type_id integer NOT NULL,
    type_name character varying
);


ALTER TABLE public.product_types OWNER TO service_role;

--
-- TOC entry 181 (class 1259 OID 16427)
-- Name: product_types_type_id_seq; Type: SEQUENCE; Schema: public; Owner: service_role
--

CREATE SEQUENCE public.product_types_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_types_type_id_seq OWNER TO service_role;

--
-- TOC entry 2138 (class 0 OID 0)
-- Dependencies: 181
-- Name: product_types_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: service_role
--

ALTER SEQUENCE public.product_types_type_id_seq OWNED BY public.product_types.type_id;


--
-- TOC entry 184 (class 1259 OID 16451)
-- Name: products; Type: TABLE; Schema: public; Owner: service_role
--

CREATE TABLE public.products (
    product_id uuid NOT NULL,
    type_id integer
);


ALTER TABLE public.products OWNER TO service_role;

--
-- TOC entry 2000 (class 2604 OID 16432)
-- Name: product_types type_id; Type: DEFAULT; Schema: public; Owner: service_role
--

ALTER TABLE ONLY public.product_types ALTER COLUMN type_id SET DEFAULT nextval('public.product_types_type_id_seq'::regclass);


--
-- TOC entry 2128 (class 0 OID 16438)
-- Dependencies: 183
-- Data for Name: artists; Type: TABLE DATA; Schema: public; Owner: service_role
--

COPY public.artists (artist_id, first_name, last_name, stage_name) FROM stdin;
1844e72d-2e3b-4518-8b19-38e657e7076c	Jean	Sibellius	Jean
83690d79-b67c-40e3-aca8-9f859046f044	Olufela	Sowande	Fela
a8fbaff5-e4cb-49d5-88d5-b70954a86986	John	Smith	Artsy
\.


--
-- TOC entry 2130 (class 0 OID 16461)
-- Dependencies: 185
-- Data for Name: merchendise; Type: TABLE DATA; Schema: public; Owner: service_role
--

COPY public.merchendise (product_id, artist_id, name) FROM stdin;
814fdef7-0108-4983-bacf-64de2e19669c	1844e72d-2e3b-4518-8b19-38e657e7076c	Commemorative hoodie
bc7c3ad0-b6d0-4e89-af37-e446af734372	1844e72d-2e3b-4518-8b19-38e657e7076c	Commemorative Mug
ee5430b8-3ff9-4a73-853b-7c7251e25886	a8fbaff5-e4cb-49d5-88d5-b70954a86986	Commemorative Mug
f70946f5-b5aa-43c2-8f40-96f8a450dd87	a8fbaff5-e4cb-49d5-88d5-b70954a86986	Commemorative hoodie
1c38c5ec-ec6f-4eb0-9a59-e73a7fa7497e	a8fbaff5-e4cb-49d5-88d5-b70954a86986	Commemorative t-shirt
efa08c3c-d128-4344-a0c9-b1915ce86700	83690d79-b67c-40e3-aca8-9f859046f044	Commemorative t-shirt
a72b20df-6b91-41a7-aad3-a4be12869748	1844e72d-2e3b-4518-8b19-38e657e7076c	Sibelius Concert
c8b92a9d-1815-4ef5-8914-4a240818953c	1844e72d-2e3b-4518-8b19-38e657e7076c	Ocherstral favourites
4c3943ef-114b-4858-b10b-b2be3b7f59dd	83690d79-b67c-40e3-aca8-9f859046f044	La fleur
47d0c7cd-3940-4347-959d-9e7723253acb	a8fbaff5-e4cb-49d5-88d5-b70954a86986	Chief is still chief
\.


--
-- TOC entry 2127 (class 0 OID 16429)
-- Dependencies: 182
-- Data for Name: product_types; Type: TABLE DATA; Schema: public; Owner: service_role
--

COPY public.product_types (type_id, type_name) FROM stdin;
1	Albums
2	Hoodies
3	T-shirts
4	Mugs
\.


--
-- TOC entry 2129 (class 0 OID 16451)
-- Dependencies: 184
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: service_role
--

COPY public.products (product_id, type_id) FROM stdin;
a72b20df-6b91-41a7-aad3-a4be12869748	1
c8b92a9d-1815-4ef5-8914-4a240818953c	1
4c3943ef-114b-4858-b10b-b2be3b7f59dd	1
47d0c7cd-3940-4347-959d-9e7723253acb	1
814fdef7-0108-4983-bacf-64de2e19669c	2
f70946f5-b5aa-43c2-8f40-96f8a450dd87	2
efa08c3c-d128-4344-a0c9-b1915ce86700	3
1c38c5ec-ec6f-4eb0-9a59-e73a7fa7497e	3
bc7c3ad0-b6d0-4e89-af37-e446af734372	4
ee5430b8-3ff9-4a73-853b-7c7251e25886	4
\.


--
-- TOC entry 2139 (class 0 OID 0)
-- Dependencies: 181
-- Name: product_types_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: service_role
--

SELECT pg_catalog.setval('public.product_types_type_id_seq', 1, false);


--
-- TOC entry 2004 (class 2606 OID 16445)
-- Name: artists artists_pkey; Type: CONSTRAINT; Schema: public; Owner: service_role
--

ALTER TABLE ONLY public.artists
    ADD CONSTRAINT artists_pkey PRIMARY KEY (artist_id);


--
-- TOC entry 2008 (class 2606 OID 16468)
-- Name: merchendise merchendise_pkey; Type: CONSTRAINT; Schema: public; Owner: service_role
--

ALTER TABLE ONLY public.merchendise
    ADD CONSTRAINT merchendise_pkey PRIMARY KEY (product_id, artist_id);


--
-- TOC entry 2002 (class 2606 OID 16437)
-- Name: product_types product_types_pkey; Type: CONSTRAINT; Schema: public; Owner: service_role
--

ALTER TABLE ONLY public.product_types
    ADD CONSTRAINT product_types_pkey PRIMARY KEY (type_id);


--
-- TOC entry 2006 (class 2606 OID 16455)
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: service_role
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (product_id);


--
-- TOC entry 2011 (class 2606 OID 16474)
-- Name: merchendise merchendise_artist_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: service_role
--

ALTER TABLE ONLY public.merchendise
    ADD CONSTRAINT merchendise_artist_id_fkey FOREIGN KEY (artist_id) REFERENCES public.artists(artist_id);


--
-- TOC entry 2010 (class 2606 OID 16469)
-- Name: merchendise merchendise_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: service_role
--

ALTER TABLE ONLY public.merchendise
    ADD CONSTRAINT merchendise_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.products(product_id);


--
-- TOC entry 2009 (class 2606 OID 16456)
-- Name: products products_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: service_role
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_type_id_fkey FOREIGN KEY (type_id) REFERENCES public.product_types(type_id);


-- Completed on 2018-06-22 00:58:24 EEST

--
-- PostgreSQL database dump complete
--

