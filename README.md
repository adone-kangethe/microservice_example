# README #

### What is this repository for? ###

* This repository showcases a simple microservice implementation done in flask and docker


### Deployment instructions ###

* Setup the database using db_schema.sql. The service was built for postgres databases
* Cd into the backend service folder and run the build_image.sh script
* Cd into the backend/application folder and edit the config.ini file to the correct values
* Edit the backend/run_container.sh script HOST and PORT values to reflect the values you wish to run from
* Run the run the backend/run_container script

* Cd into the frontend folder and run the build_image.sh script
* Cd into the frontend/application folder and edit the config file to the correct values for backend host and port
* Edit the frontend/run_container.sh script for the correct host and port values
* Run the frontend/runc_container.sh script
