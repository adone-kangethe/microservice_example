#!/bin/bash
HOST=0.0.0.0
PORT=5000

docker run -d --name backend --rm \
	--mount type=bind,source="$(pwd)"/application/config.ini,target=/application/config.ini\
	 -p $HOST:$PORT:5000 backend_service
