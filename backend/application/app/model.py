from sqlalchemy import Column, String, INT, ForeignKeyConstraint, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from app import db


class Artist(db.Model):
    __tablename__ = 'artists'
    artist_id = Column(UUID(as_uuid=True), primary_key=True)
    first_name = Column(String)
    last_name = Column(String)
    stage_name = Column(String)


class Merch(db.Model):
    __tablename__ = 'merchendise'
    product_id = Column(UUID(as_uuid=True), ForeignKey('products.product_id'), primary_key=True)
    artist_id = Column(UUID(as_uuid=True), ForeignKey('artists.artist_id'), primary_key=True)
    name = Column(String)


class Product(db.Model):
    __tablename__ = 'products'
    product_id = Column(UUID(as_uuid=True), primary_key=True)
    type_id = Column(INT, ForeignKey('product_types.type_id'))


class ProductTypes(db.Model):
    __tablename__ = 'product_types'
    type_id = Column(INT, primary_key=True)
    type_name = Column(String)

db.create_all()