import json
from flask import Flask, request, send_from_directory
from flask_restful import Resource, Api, reqparse
from flask_sqlalchemy import SQLAlchemy

from app.config import read_configs, get_db_url

app = Flask(__name__)
api = Api(app)
configs=read_configs('config.ini')

app.config['SQLALCHEMY_DATABASE_URI'] = get_db_url(configs)

db = SQLAlchemy(app)


class Artists(Resource):
    def get(self):
        from app.model import Artist
        artists = db.session\
            .query(Artist.artist_id,
                   Artist.first_name,
                   Artist.last_name,
                   Artist.stage_name)\
            .all()

        if artists:
            artist_dict_list = []
            for artist in artists:
                d = {'id': str(artist.artist_id),
                     'first_name': artist.first_name,
                     'last_name': artist.last_name,
                     'stage_name': artist.stage_name}
                artist_dict_list.append(d)
            return json.dumps(artist_dict_list)

        return json.dumps({'message': 'no artists found'})


class ArtistMerch(Resource):
    def get(self, artist_id):
        from app.model import Artist, Merch
        from app.model import Product
        from app.model import ProductTypes
        merchs = db.session\
            .query(Merch.name,
                   Product.product_id,
                   ProductTypes.type_name
                                  )\
            .filter(Merch.artist_id == artist_id)\
            .filter(Product.product_id == Merch.product_id)\
            .filter(ProductTypes.type_id == Product.type_id)\
            .all()
        if merchs:
            merch_dict = {}
            for merch in merchs:
                if merch.type_name not in merch_dict:
                    merch_dict[merch.type_name] = []
                d = {'name': merch.name,
                     'product_id': str(merch.product_id)}
                merch_dict[merch.type_name].append(d)

            return json.dumps(merch_dict, sort_keys=True)

        return json.dumps({'message': 'no artists found'})

class Images(Resource):
    def get(self):

        parser = reqparse.RequestParser()
        parser.add_argument('type')
        parser.add_argument('id')
        args = parser.parse_args()
        if args['type'] == 'artist':
            return send_from_directory('static/images/artists/', str(args['id']),
                                       attachment_filename='artist.jpg' ,  as_attachment=True)
        if args['type'] == 'product':
            return send_from_directory('static/images/products/', str(args['id']),
                                       attachment_filename='product.jpg', as_attachment=True)


api.add_resource(Artists, '/artists')
api.add_resource(ArtistMerch, '/artist_merch/<artist_id>')
api.add_resource(Images, '/images')

