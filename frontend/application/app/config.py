def read_configs(path):
    config_dict = {}
    with open(path, 'r') as file:

        for line in file:
            key, value = line.split('=')
            # this returns only the string without a trailing newline character
            config_dict[key] = value.split('\n')[0]
    return config_dict

