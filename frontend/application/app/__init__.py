import ast
import json
import requests
from flask import Flask, render_template

from app.config import read_configs

configs = read_configs('config.ini')

app = Flask(__name__)

backend_url = 'http://'+configs['BACKEND_HOST']+":" \
            + configs['BACKEND_PORT']

print backend_url
@app.route("/")
def hello():
    artists=requests.get(backend_url+'/artists')
    artists_dict = json.loads(ast.literal_eval(artists.content))
    return render_template("home_template.html",
                           artists=artists_dict,
                           backend=backend_url)


@app.route("/merch/<string:artist_id>")
def merch(artist_id):
    merch = requests.get(backend_url+'/artist_merch/'+artist_id)
    merch_dict = json.loads(ast.literal_eval(merch.content))
    return render_template("merch_template.html",
                           products=merch_dict,
                           backend=backend_url)