#!/bin/bash
HOST=0.0.0.0
PORT=5001

docker run  -d --name frontend --rm \
	--mount type=bind,source="$(pwd)"/application/config.ini,target=/application/config.ini,readonly\
	 -p $HOST:$PORT:5000 frontend_service
